#!/bin/bash

if [ -z "$1" ]; then
        echo    "Please enter the root of all data (e.g. arcade/pooyan)"
        exit
fi

if [ -z "$2" ]; then
        echo    "Please enter the name of the application (e.g. JSPWikiTotal)"
        exit 
fi
if [ -z "$3" ]; then
	echo	"You have decide to put build folder empty"
fi
if [ -z "$4" ]; then
	echo	"You have decided to put src folder empty"
fi
rootDir="$1"
app="$2"
binaryDir="binaryFiles"
analDir="BCE"
malletVersion="mallet-2.0.7"
buildDir="$3"
srcDir="$4"

echo "Copying all necessary files to arcade/tmp folder..."
#this will copy all necessary files to the tmp folder
cp $rootDir/$binaryDir/$app/base/* tmp/

echo "Running ARC..."
#this will run arc
#java -jar BatchClusteringEngine.jar $rootDir/$binaryDir/$app/codes/ $rootDir/$analDir/$app "build" "src"
ls $rootDir/$binaryDir/$app/codes/ | xargs -P 24 -I % java -jar BatchClusteringEngine.jar $rootDir/$binaryDir/$app/codes/% $rootDir/$analDir/$app "$buildDir" "$srcDir"
