#!/bin/bash

if [ -z "$1" ]; then
        echo    "Please enter the root of all data (e.g. arcade/pooyan)"
        exit
fi

if [ -z "$2" ]; then
        echo    "Please enter the name of the application (e.g. JSPWikiTotal)"
        exit 
fi

rootDir="$1"
app="$2"
binaryDir="binaryFiles"
analDir="BCE"
malletVersion="mallet-2.0.7"

echo "Removing the base folder of the application..."
#the base folder is the location where pipe, inference, and topic_model artifacts of an application is collected
rm $rootDir/$binaryDir/$app/base/*

echo "PipeExtractor ..."
#the following command extract pipe from the set of selected versions of a software, it will be the set of minor versions
java -jar PipeExtractor.jar "$rootDir/$binaryDir/$app/selected/" "$rootDir/$binaryDir/$app/base"

echo "Copying the output.pipe which is in the father folder of base..."
#since the scripts are windows based, it might not generate the pipe file in the most appropriate folder
mv $rootDir/$binaryDir/$app/*.pipe $rootDir/$binaryDir/$app/base/output.pipe

echo "Generating topicmodel.data..."
# this will generate topic model of versions in the set of selected versions
$rootDir/$malletVersion/bin/mallet import-dir --input "$rootDir/$binaryDir/$app/selected/" --remove-stopwords TRUE --keep-sequence TRUE --output "$rootDir/$binaryDir/$app/base/topicmodel.data"

echo "Generating infer.mallet..."
#this will geneated inference file of the versions in the set of selected versions, from the topic model
$rootDir/$malletVersion/bin/mallet train-topics  --input "$rootDir/$binaryDir/$app/base/topicmodel.data"  --inferencer-filename "$rootDir/$binaryDir/$app/base/infer.mallet" --num-top-words 50  --num-topics 100  --num-threads 3  --num-iterations 100  --doc-topics-threshold 0.1

echo "Running ARC..."
#this will run arc
#java -jar BatchClusteringEngine.jar $rootDir/$binaryDir/$app/codes/ $rootDir/$analDir/$app "build" "src"
#ls $rootDir/$binaryDir/$app/codes/ | xargs -P 30 -I % java -jar BatchClusteringEngine.jar $rootDir/$binaryDir/$app/codes/% $rootDir/$analDir/$app "build" "src"
